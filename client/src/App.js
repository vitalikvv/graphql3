import React from 'react'
import './App.css';
import Preloader from "./components/Preloader/Preloader";
import {useDispatch, useSelector} from "react-redux";
import MainHeader from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import {Redirect, Route, Switch} from "react-router-dom";
import Chat from "./components/Chat/Chat";
import Toast from "react-toast-component";
import {displayErrorMessage, setLikeOrDis} from "./store/messagesList";

function App() {

    const { likeOrDisValue, startLoading, errorMessage } = useSelector(state => ({
        errorMessage: state.chat.errorMessage,
        startLoading: state.chat.startLoading,
        likeOrDisValue: state.chat.likeOrDisValue,
    }));
    const dispatch = useDispatch();

    return (
        <>
            {startLoading ? <Preloader/> : null}
            <MainHeader/>
            <main>
                <Switch>
                    <Route path="/" exact render={() => <Redirect to='/chat'/>}/>
                    <Route path="/chat" render={() => <Chat/>}/>
                    <Route path="*" exact render={() =>
                        <div style={{
                            marginTop: '90px',
                            flex: '1 0 auto',
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                            fontSize: '30px'
                        }}
                        >NOT FOUND</div>
                    }/>
                </Switch>
            </main>
            <Footer/>
            <Toast
                isOpen={errorMessage || likeOrDisValue}
                hasAutoDismiss={true}
                closeCallback={() => {
                    dispatch(displayErrorMessage(null))
                    dispatch(setLikeOrDis(null))
                }}
                description={errorMessage
                    ? errorMessage
                    : likeOrDisValue === 'like' ? 'You liked post.' : 'You disliked post.'}
                title="Notification!"
                duration={3000}
                classNames={errorMessage
                    ? ['error', 'notification-error']
                    : likeOrDisValue === 'like'
                        ? ['success', 'notification-message-like']
                        : ['error', 'notification-message-dislike']
                }
            />
        </>
    );
}

export default App;
