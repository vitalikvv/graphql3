import { configureStore } from '@reduxjs/toolkit';
import {chat} from "./messagesList";

export const store = configureStore({
    reducer: {
        chat
    }
});

window.store= store