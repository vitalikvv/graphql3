import React, {useEffect} from 'react'
import {Button, Comment, Icon} from "semantic-ui-react";
import './Message.css'
import ModalWindow from "../Modal/Modal";
import {useDispatch, useSelector} from "react-redux";
import {setOpenModal} from "../../store/messagesList";

const Message = ({message, likeMessage}) => {
    const { isOpenModal, repliedMessageText } = useSelector(state => ({
        isOpenModal: state.chat.isOpenModal,
        repliedMessageText: state.chat.repliedMessageText
    }));

    const dispatch = useDispatch();
    const openModal = React.useCallback((isOpen, repliedMessageText) => (
        dispatch(setOpenModal(isOpen, repliedMessageText))
    ), [dispatch]);

    return (
        <div className='message'>
            <Comment.Group>
                <Comment style={{display: 'flex', justifyContent: 'space-between', alignItems:'center'}}>
                    <Comment.Content>
                        <Comment.Author className='message-user-name'>#{message.id}</Comment.Author>
                        <Comment.Text className='message-text'>
                            {message.text}
                        </Comment.Text>
                        <Comment.Actions>
                            <Comment.Action
                                style={{marginRight: 0}}
                                onClick={() => likeMessage('like')}
                                className={message.likesCount > 0 ? 'message-liked' : 'message-like'}
                            >
                                <Icon name='thumbs up' /></Comment.Action>
                            <Comment.Metadata style={{ width: '10px', marginLeft:0, marginRight: '5px' }}>
                                <span>{message.likesCount > 0 ? message.likesCount : null}</span>
                            </Comment.Metadata>
                            <Comment.Action
                                style={{marginRight: 0}}
                                onClick={() => likeMessage('dislike')}
                                className={message.likesCount > 0 ? 'message-liked' : 'message-like'}
                            >
                                <Icon name='thumbs down' /></Comment.Action>
                            <Comment.Metadata style={{ width: '10px', marginLeft:0, marginRight: '5px' }}>
                                <span>{message.likesCount > 0 ? message.likesCount : null}</span>
                            </Comment.Metadata>
                        </Comment.Actions>
                        <Comment.Metadata style={{ marginLeft:0 }}>
                            <div className='message-time'>{message.createdAt.split(' ')[1]}</div>
                        </Comment.Metadata>
                    </Comment.Content>
                    <Button
                        style={{height: 'fit-content', display:'flex'}}
                        content='Reply'
                        icon='edit'
                        color='teal'
                        onClick={() => openModal(true, message.text)}
                    />
                </Comment>
            </Comment.Group>
            {isOpenModal && <ModalWindow
                messageId={message.id}
                text={repliedMessageText}
                close={() => openModal(false, '')}
            />
            }
        </div>
    )
}

export default Message;