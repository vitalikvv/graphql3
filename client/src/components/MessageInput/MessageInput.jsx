import React, {useState} from 'react'
import {Button, Container, Form, TextArea} from "semantic-ui-react";
import './MessageInput.css'
import { gql, useMutation } from '@apollo/client';
import {POST_MESSAGE_MUTATION} from "../../Queries/queries";

const MessageInput = ({addNewMessage, currentUser}) => {
    const [value, setValue] = useState('')
    const [createMessage] = useMutation(POST_MESSAGE_MUTATION);

    const changeHandler = (e) => {
        setValue(e.target.value)
    }

    const clickHandler = () => {
        if (value.length > 0) {
            createMessage({ variables: { text: value } });
            setValue('')
        }
    }

    return (
        <div className='message-input'>
            <Container style={{width: '1200px'}}>
                <Form style={{marginBottom: '20px'}}>
                    <TextArea
                        value={value}
                        onChange={changeHandler}
                        as='textarea'
                        rows={3}
                        className='message-input-text'
                        label='Input your message'
                        placeholder='Tell us something...'
                    />
                </Form>
                <Button
                    className='message-input-button'
                    onClick={clickHandler}
                >
                    Submit
                </Button>
            </Container>
        </div>
    )
}

export default MessageInput