import React, {createRef, useEffect} from 'react'
import {Divider, Grid, Header, Icon, Rail, Segment, Sticky, Ref} from 'semantic-ui-react'
import './MessageList.css'
import Message from "../Message/Message";
import {getMonth, getWeekDay} from "../../helpers/changeDateFormat";
import MessageInput from "../MessageInput/MessageInput";
import Preloader from "../Preloader/Preloader";

const MessageList = (props) => {
    const contextRef = createRef()

    useEffect(() => {
        props.subscribeToNewComments();
    }, [props.subscribeToNewComments])

    if (props.loading) return <Preloader/>;
    if (props.error) return <p>Error :(</p>;

    let countOfUsers;
    let lastDate;
    let countOfMessages = 0;

    if (props.data.messages.messageList.length > 0) {
        const set = new Set();
        props.data.messages.messageList.forEach(message => {
            set.add(message.Id);
            countOfMessages++;
        })
        countOfUsers = set.size
        const tzoffset = (new Date()).getTimezoneOffset() * 60000;

        lastDate = props.data.messages.messageList[props.data.messages.messageList.length - 1].createdAt;
        lastDate = (new Date(Date.parse(lastDate) - tzoffset)).toISOString()
        lastDate = lastDate.split('T').join(' ').split('.')[0]

    } else {
        return
    }

    const onLikeMessage = (typeLikeOrDis) => {
        console.log(typeLikeOrDis)
    }

    let date = '';
    const now = new Date();
    const millisecondsInDay = 86400000;

    return (
        <div className='message-list'>
            <Grid centered columns={2}>
                <Grid.Column>
                    <Ref innerRef={contextRef}>
                        <Segment>
                            {props.data.messages.messageList.map((message) => {
                                    const changedDate = message.createdAt.split('T').join(' ').split('.')[0]
                                    const yyyymmddArray = changedDate.split(' ')[0].split('-')
                                    const ddmmyyyy = yyyymmddArray.reverse().join('-')

                                    if (date !== ddmmyyyy) {
                                        date = ddmmyyyy
                                        const dateParts = ddmmyyyy.split('-');
                                        const dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
                                        const timeDiff = new Date(now) - new Date(dateObject);

                                        let dateToDisplay = getWeekDay(new Date(dateObject)) + `, ${dateParts[0]} ` + getMonth(new Date(dateObject)) + ` ${dateParts[2]}`

                                        if (timeDiff < millisecondsInDay) {
                                            dateToDisplay = 'Today'
                                        }

                                        if (timeDiff >= millisecondsInDay && timeDiff < millisecondsInDay * 2) {
                                            dateToDisplay = 'Yesterday'
                                        }
                                        return (
                                            <div key={message.id}>
                                                <Divider className='messages-divider'
                                                         horizontal>{dateToDisplay}</Divider>
                                                    <Message
                                                        message={message}
                                                        likeMessage={onLikeMessage}
                                                    />
                                            </div>
                                        )
                                    } else {
                                        return (
                                            <div key={message.id}>
                                                    <Message
                                                        message={message}
                                                        likeMessage={onLikeMessage}
                                                    />
                                            </div>
                                        )
                                    }
                                }
                            )}
                            <Rail position='right'>
                                <Sticky
                                    bottomOffset={50}
                                    context={contextRef}
                                    offset={90}
                                >
                                    <div className='right-sticky'>
                                        <Header as='h3'>Сhat statistics</Header>
                                        <div className='header-users-count'>
                                            <Icon name='user'/>
                                            {countOfUsers} participants
                                        </div>
                                        <Divider/>
                                        <div className='header-messages-count'>
                                            <Icon name='mail'/>
                                            {countOfMessages} messages
                                        </div>
                                        <Divider/>
                                        <div className='header-last-message-date'>
                                            <Icon name='calendar'/>
                                            Last message at:
                                            <br/>
                                            {lastDate}
                                        </div>
                                    </div>
                                </Sticky>
                            </Rail>
                        </Segment>
                    </Ref>
                    <MessageInput />
                </Grid.Column>
            </Grid>
        </div>
    )
}

export default MessageList