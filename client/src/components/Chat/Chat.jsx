import React from 'react'
import MessageList from "../MessageList/MessageList";
import {useQuery} from "@apollo/client";
import {MESSAGE_QUERY, NEW_MESSAGES_SUBSCRIPTION} from "../../Queries/queries";


const Chat = () => {

    const { subscribeToMore, ...result } = useQuery(MESSAGE_QUERY);

    return (
        <div className='chat'>
            <MessageList
                {...result}
                subscribeToNewComments={() =>
                    subscribeToMore({
                        document: NEW_MESSAGES_SUBSCRIPTION,
                        variables: { text: '' },
                        updateQuery: (prev, { subscriptionData }) => {
                            if (!subscriptionData.data) return prev;
                            const newFeedItem = subscriptionData.data.newMessage;
                            const newMessageList = [...prev.messages.messageList]
                            newMessageList.push(newFeedItem)
                            const newState = {
                                ...prev,
                                messages: {
                                    ...prev.messages,
                                    messageList: newMessageList
                                }
                            }
                            return newState;
                        }
                    })
                }
            />
        </div>
    )
}

export default Chat