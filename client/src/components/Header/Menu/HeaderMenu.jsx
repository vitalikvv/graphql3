import React, {useEffect, useState} from 'react'
import {Icon, Menu} from 'semantic-ui-react'
import {Link, useLocation} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {displayErrorMessage} from "../../../store/messagesList";

const HeaderMenu = () => {
    const { currentUser, ownLastMessage } = useSelector(state => ({
        currentUser: state.chat.currentUser,
        ownLastMessage: state.chat.ownLastMessage,
    }));
    const dispatch = useDispatch();
    let location = useLocation();

    useEffect(()=> {
        setActiveItem(location.pathname)
    },[location.pathname])
    const [activeItem, setActiveItem] = useState(location.pathname)

    const handleItemClick = (e, {name}) => {
        if (!ownLastMessage && name === '/message_editor') {
            dispatch(displayErrorMessage('You dont have any message.'));
        }
        setActiveItem(name)
    }

    return (
        <Menu fluid icon='labeled' widths={4} style={{margin: 0, height: '100%', borderRadius: '0px'}}>
            <Menu.Item
                as={Link}
                to="/chat"
                name='/chat'
                active={activeItem === '/chat'}
                onClick={handleItemClick}
                style={{height: '100%'}}
            >
                <Icon name='wechat'/>
                Chat
            </Menu.Item>
        </Menu>
    )
}

export default HeaderMenu