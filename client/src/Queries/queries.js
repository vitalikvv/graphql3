import { gql } from "@apollo/client";

export const MESSAGE_QUERY = gql`
    query messageQuery($orderBy: MessagesOrderByInput) {
        messages(orderBy: $orderBy) {
            count
            messageList {
                id
                text
                createdAt
            }
        }
    }
`;

export const POST_MESSAGE_MUTATION = gql`
    mutation PostMutation($text: String!) {
        postMessage(text: $text) {
            id
            text
        }
    }
`;

export const NEW_MESSAGES_SUBSCRIPTION = gql`
    subscription {
        newMessage {
            id
            text
        }
    }
`;