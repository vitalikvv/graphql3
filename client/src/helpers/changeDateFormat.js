
export function changeDateFormat(messages) {

    messages.forEach(message => {
        const changedDate = message.createdAt.split('T').join(' ').split('.')[0]
        const yyyymmddArray = changedDate.split(' ')[0].split('-')
        const ddmmyyyy = yyyymmddArray.reverse().join('-')
        const hhmm = changedDate.split(' ')[1].substr(0, 5)
        message.createdAt_ = ddmmyyyy + ' ' + hhmm

        message.likesCount = 0;
    })

    return messages;
}


export function getWeekDay(date) {
    let days = ['Sunday', 'Monday', 'Thursday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

    return days[date.getDay()];
}

export function getMonth(date) {
    let days = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    return days[date.getMonth()];
}