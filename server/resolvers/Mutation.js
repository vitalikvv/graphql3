function postMessage(parent, args, context, info) {
    return context.prisma.createMessage ({
        text: args.text,
    })
}

async function postLike (parent, args, context, info) {
    const messageExisted = await context.prisma.$exists.message({
        id: args.messageId
    });
    if (!messageExisted) {
        throw new Error (`Message with ID ${args.messageId} does not exist`)
    }

    return context.prisma.createLike ({
        message: { connect: { id: args.messageId}}
    });
}

async function postDislike (parent, args, context, info) {
    const messageExisted = await context.prisma.$exists.message({
        id: args.messageId
    });
    if (!messageExisted) {
        throw new Error (`Message with ID ${args.messageId} does not exist`)
    }

    return context.prisma.createDislike ({
        message: { connect: { id: args.messageId}}
    });
}

module.exports = {
    postMessage,
    postLike,
    postDislike
}
