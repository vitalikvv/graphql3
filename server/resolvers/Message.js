function message(parent, args, context, info) {
    return context.prisma.like ({
        id: parent.id
    }).message();
}

module.exports = {
    message
}