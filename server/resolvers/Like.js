function likes(parent, args, context, info) {
    return context.prisma.message ({
        id: parent.id
    }).likes();
}

module.exports = {
    likes
}