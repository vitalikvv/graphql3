const {GraphQLServer} = require('graphql-yoga')
const {prisma} = require('./prisma/generated/prisma-client')
const Query = require('./resolvers/Query')
const Mutation = require('./resolvers/Mutation')
const Subscription = require('./resolvers/Subscription')

const resolvers = {
    Query,
    Mutation,
    Subscription,
/*    Message: {
        ...require('./resolvers/Message')
    },
    Like: {
        ...require('./resolvers/Like')
    }*/
};

const server = new GraphQLServer({
    typeDefs: './schema.graphql',
    resolvers,
    context: {prisma}
})

server.start(() => console.log('http://localhost:4000'))